<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test-123', function () {
    $arry = ['Pakistan', 'USA', 'UAE'];
    return view('test-123', ['arry' => $arry]);
});
Route::post('test-999', function (Illuminate\Http\Request $request) {
    $forData = $request->all();
    dd($forData);
})->name("test999");

